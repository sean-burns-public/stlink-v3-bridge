# STLINK-V3 Bridge Software (SPI Interface Only)

This software is based on the original software provided by ST Microelectronics. This adaptation is only compatible with Linux. It has been tested on Ubuntu 20.04.

## Building

```bash
cd <this-repository>
make
```

## Running

First, ensure an STLINK-V3 is connected to a USB port of the Linux machine and then:

```bash
cd <this-repository>
./build/bin/spi.bin
```

## Clean-up

```bash
cd <this-repository>
make clean
```
